<?php

/**
 * @file
 * Various callbacks and helper functions
 */

/**
 * ImageCache 'individual_rotate' action.
 */
function individual_rotate_image(&$image, $data = array()) {
  // Find rotate mapping, if any
  if (! $degrees = _individual_rotate_get_rotation($image->source)) {
    return TRUE;
  }
  // Rotate
  imageapi_image_rotate($image, $degrees);
  return TRUE;
}

/**
 * Wrapper function around desired hash generator
 */
function _individual_rotate_filename_hash($filename) {
  // @todo: Make filename be relative to drupal root.
  return hash('md5', $filename);
}

/**
 * Make rotation one of 0, 90, 180 or 270
 */
function _individual_rotate_normalize_rotation($degrees) {
  $degrees = (int) $degrees;
  while ($degrees >= 360) $degrees -= 360;
  while ($degrees < 0)    $degrees += 360;
  return $degrees - ($degrees % 90);
}

/**
 * Given the drupal path to a file, return the current image rotation
 */
function _individual_rotate_get_rotation($filepath) {
  $rotation = db_result(db_query("SELECT degrees FROM {individual_rotate_map} WHERE hash = '%s'", _individual_rotate_filename_hash($filepath)));
  return $rotation ? $rotation : 0;
}

/**
 * Menu callback for js calls
 *
 * @param string $op
 *   Operation. Either 'relative' or 'absolute'.
 * @param int $fid
 *   File ID.
 * @param int $degrees
 *   Degrees to rotate. Either relative from previous or absolute.
 */
function individual_rotate_js($op, $fid, $degrees = 0) {
  global $user;

  $ret['status'] = FALSE;
  $fid = (int) $fid;
  $degrees = (int) $degrees;

  $file = db_fetch_array(db_query("select * from {files} where fid = %d", $fid));

  if (! $file) {
    $ret['message'] = t('File not found');
    _individual_rotate_ajax_finish($ret);
  }

  if (! user_access('rotate all images') && $file['uid'] != $user->uid) {
    $ret['message'] = 'Access denied';
    _individual_rotate_ajax_finish($ret);
  }

  $ret['status'] = TRUE;
  if ($op != 'preview' && $degrees % 360 === 0) {
    $ret['message'] = t('No rotation required');
    _individual_rotate_ajax_finish($ret);
  }

  switch ($op) {
    case 'preview':
      $ret['data'] = theme('imagefield_rotate_admin_thumbnail', $file, $degrees);
      break;
    case 'relative':
    case 'absolute':
      $new_rotation = (($op == 'relative' && $previous_record ? $previous_record['degrees'] : 0) + $degrees);
      if (! individual_rotate_save_rotation($file, $new_rotation)) {
        $ret['status'] = FALSE;
        $ret['message'] = t('Failed to rotate image');
      }
      else {
        $ret['data']['degrees'] = $record['degrees'];
        $ret['message'] = t('Image rotated !degree degrees', array('!degree' => $degrees));
      }
      break;
  }
  _individual_rotate_ajax_finish($ret);
}

function _individual_rotate_ajax_finish($ret) {
  print drupal_to_js($ret);
  exit;
}
