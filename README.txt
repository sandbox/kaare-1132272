
Rotate imagefields individually using imagecache for rotation and
imagefield widgets as UI.

This module lets you rotate images 90°, 180° and 270° on a per image
basis.  Images rotation are set in the imagefield node form and the
actual rotation is done by imagecache.


DEPENDENCIES

  o ImageField
  o ImageCache


USAGE

  o Enable the modules ImageCache -> "Individual Rotate" and
    CCK -> "Imagefield Rotate" as part of this package.

  o Add the ImageCache action "Individual Rotate" for your
    presets.  This action should be before any actions altering the
    canvas size.

  o Until #372497 is fixed, enable the module ImageCache -> "Imagecache
    Rotation Canvas size Fix", and add it's action "Rotation Canvas
    Size fix" immediately after the "Individual Rotate" ImageCache
    action.

  o Configure your image fields to have rotation enabled

  o Edit your image nodes and reload the rendered image (pass browser
    cache).


LIMITATIONS

Since there is no mapping from filename to {files}.fid, having several
filefield/imagefield referencing the same file (as you might with
filefield_sources.module), will cause trouble.  This module uses a md5 hash of
the filename to acquire the fid of the file.  As such, multiple references to
the same field will cause only one of the configured fid's to act as rotate
configuration for all filefields.
