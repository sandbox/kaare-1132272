
This is a workaround for a bug in the ImageMagick implementation of
ImageAPI during rotation of images in combination with imagecache. See
issue #372497 [1]. Until this issue is solved, this module will
suffice.

It's an ImageCache action that need to be added immediately after any
image rotation action, and before actions that alters the canvas size.
So the Imagecache pipeline should look something like this:


Action                 | Settings
----------------------------------
Rotate                 | 15°
Rotate Canvas Size Fix |
Scale                  | 512x320

It doesn't add any action to the imagecache pipeline, it just looks
for previous rotation actions and updates the canvas size accordingly.

Thanks to dman [2] for the canvas size calculation algorithm.


[1] http://drupal.org/node/372497
[2] http://drupal.org/user/33240
