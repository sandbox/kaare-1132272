Drupal.imagefield_rotate = {
    cache: {}
};

Drupal.behaviors.imagefield_rotate = function()
{
    $("#node-form .filefield-element:has(input[name$='\[imagefield_rotate_rotation\]'])").each(function() {
	var field = $(this);
	var fid = field.find("input[name$='\[fid\]']").val();
	// Cache existing selection
	var old_degrees = field.find("input[name$='\[imagefield_rotate_rotation\]']:checked").val();
	Drupal.imagefield_rotate.cache['fid-'+ fid +'-'+ old_degrees] = field.find('#imagefield-preview-fid-'+ fid).html();

	// Update preview html/image when a different rotation is selected
	field.find("input[name$='\[imagefield_rotate_rotation\]']").change(function() {
	    var degrees = $(this).val();
	    var ckey = 'fid-'+ fid +'-'+ degrees;
	    if (Drupal.imagefield_rotate.cache[ckey]) {
		field.find('#imagefield-preview-fid-'+ fid).html(Drupal.imagefield_rotate.cache[ckey]);
		return;
	    }
	    $.ajax({
		dataType: 'json',
		url: Drupal.settings.basePath +'imagefield_rotate/js/preview/'+ fid +'/'+ degrees,
		success: function(data, status, http_request) {
		    field.find('#imagefield-preview-fid-'+ fid).html(data.data);
		    Drupal.imagefield_rotate.cache[ckey] = data.data;
		}
	    });
	});
    });
};
