<?php

/**
 * @file
 * Various callbacks and helper functions
 */

/**
 * Override for theme_imagefield_widget_preview()
 */
function theme_imagefield_rotate_widget_preview($item = NULL) {
  module_load_include('inc', 'individual_rotate');
  $degrees = _individual_rotate_get_rotation($item['filepath']);

  return '<div class="imagefield-preview" id="imagefield-preview-fid-'. $item['fid'] .'">'.
    theme('imagefield_rotate_admin_thumbnail', $item, $degrees) . '</div>';
}

function theme_imagefield_rotate_admin_thumbnail($item, $degrees = 0) {
  // Due to the order the imagefield form is built (thumb first, then
  // imagefield settings), we cannot use the #process handler to set whether
  // this image is enabled for rotation or not during this request, so we have
  // to do it the hard way: Load the current node and fetch field information
  // for the field matching this fid.
  if ($node = menu_get_object()) {
    $fields = content_fields(null, $node->type);

    // Search through the fields and look for matching fid
    $our_field = FALSE;
    foreach ($fields as $field_name => $field) {
      if ($field['type'] != 'filefield') {
        continue;
      }
      foreach ($node->$field_name as $field_delta) {
        if ($field_delta['fid'] == $item['fid']) {
          $our_field = $field;
          break;
        }
      }
      if ($our_field) {
        break;
      }
    }
  }

  // Determine whether to render the image ourselves. !$node means it's an
  // ajax call, so we render it.
  if (! $node or ($our_field and $our_field['widget']['imagefield_rotate'])) {
    $thumb = imagefield_rotate_preview($item['filepath'], $degrees);
    return theme('image', $thumb, $item['filename'], $item['filename'], array('id' => 'imagefield-rotate-fid-'. $item['fid']));
  }

  // Fall back to the original imagefield thumbnail rendering
  return theme('imagefield_admin_thumbnail', $item);
}

/**
 * Return a rotated image filename feasible for preview
 *
 * @param string $filename
 *   Source filename to preview
 * @param int $degrees
 *   Amount to rotate the image
 */
function imagefield_rotate_preview($filepath, $degrees) {
  // Build destination path in same directory as imagefield does.
  $short_path = preg_replace('/^' . preg_quote(file_directory_path(), '/') . '/', '', $filepath);
  $thumb = file_directory_path() . '/imagefield_thumbs/rotate/'. $degrees . $short_path;
  if (! file_exists($thumb)) {
    // Build the fixed imagecache action array. Image is first rotated, then
    // scaled, according to imagefield preview settings.
    $size = explode('x', variable_get('imagefield_thumb_size', '100x100'));
    $actions[] = array(
      'action' => 'imagecache_rotate',
      'data' => array(
        'degrees' => $degrees,
        'random'  => 0,
        'bgcolor' => '',
      ),
    );
    // @see issue http://drupal.org/node/372497
    if (module_exists('icrotfix')) {
      $actions[] = array(
        'action' => 'icrotfix_recalculate_canvas',
        'data' => array(),
      );
    }
    $actions[] = array(
      'action' => 'imagecache_scale',
      'data' => array(
        'width'   => $size[0],
        'height'  => $size[1],
        'upscale' => 0,
      ),
    );
    imagecache_build_derivative($actions, $filepath, $thumb);
  }
  return $thumb;
}

/**
 * Menu callback for js calls
 *
 * @param string $op
 *   Operation. Either 'relative' or 'absolute'.
 * @param int $fid
 *   File ID.
 * @param int $degrees
 *   Degrees to rotate. Either relative from previous or absolute.
 */
function imagefield_rotate_js($op, $fid, $degrees = 0) {
  global $user;

  $fid = (int) $fid;
  $degrees = (int) $degrees;

  $file = db_fetch_array(db_query("select * from {files} where fid = %d", $fid));

  if (! $file) {
    $ret['message'] = t('File not found');
    _imagefield_rotate_ajax_finish($ret);
  }

  if (! user_access('rotate all images') && $file['uid'] != $user->uid) {
    $ret['message'] = t('Access denied');
    _imagefield_rotate_ajax_finish($ret);
  }

  if ($op != 'preview' && $degrees % 360 === 0) {
    $ret['message'] = t('No rotation required');
    _imagefield_rotate_ajax_finish($ret, TRUE);
  }

  switch ($op) {
    case 'preview':
      $ret['data'] = theme('imagefield_rotate_admin_thumbnail', $file, $degrees);
      break;
    default:
      $ret['message'] = t('Unknown js call');
      _imagefield_rotate_ajax_finish($ret);
  }
  _imagefield_rotate_ajax_finish($ret, TRUE);
}

function _imagefield_rotate_ajax_finish($ret, $status = FALSE) {
  $ret['status'] = $status;
  print drupal_to_js($ret);
  exit;
}

